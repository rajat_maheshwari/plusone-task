const jwt = require('jsonwebtoken');
const { SERVER } = require('../config/index');

const createToken = function (tokenObj) {
	try {
		return jwt.sign(tokenObj, SERVER.jwtSecretKey);
	} catch (error) {
		console.log(error.message);
		throw new Error('Token Generate Error');
	}
};

const verifyToken = function (token) {
	try {
		return jwt.verify(token, SERVER.jwtSecretKey);
	} catch (error) {
		console.log(error.message);
		throw new Error('Invalid token');
	}
};

module.exports = {
	createToken,
	verifyToken
};