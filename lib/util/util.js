const crypto = require('crypto');

const { SERVER } = require('../config/index');

const basicAuthFunction = function (token) {
	if (token.split(' ').length !== 2) return false;
	const credentials = Buffer.from(token.split(' ')[1], 'base64').toString('ascii');
	const [username, password] = credentials.split(':');
	if (username !== SERVER.BASIC_AUTH.NAME || password !== SERVER.BASIC_AUTH.PASS) { return false; }
	return true;
};

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
*/
const genRandomString = function (length) {
	return crypto.randomBytes(Math.ceil(length / 2))
		.toString('hex') /** convert to hexadecimal format */
		.slice(0, length);   /** return required number of characters */
};

const encryptHashPassword = function (password, salt) {
	const hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
	hash.update(password);
	return hash.digest('hex');
};

const matchPassword = function (password, dbHash, salt) {
	if (!salt) return false;
	const hash = encryptHashPassword(password, salt);
	if (dbHash !== hash) {
		return false;
	} else
		return true;
};

module.exports = {
	basicAuthFunction,
	genRandomString,
	encryptHashPassword,
	matchPassword
};