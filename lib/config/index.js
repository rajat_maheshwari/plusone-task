
const constant = require('./constant');
const environment = require('./environment');

module.exports = {
	CONSTANT: constant,
	SERVER: environment
};