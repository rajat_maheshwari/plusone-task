const dotenv = require('dotenv');

dotenv.config({ path: '.env' });

const SERVER = Object.freeze({
	PORT: process.env['PORT'],
	MONGO: {
		DB_NAME: process.env['DB_NAME'],
		DB_URL: process.env['DB_URL'],
		OPTIONS: {
			// user: process.env['DB_USER'],
			// pass: process.env['DB_PASSWORD'],
			useNewUrlParser: true,
			useCreateIndex: true,
			useUnifiedTopology: true,
			useFindAndModify: false
		}
	},
	BASIC_AUTH: {
		NAME: '12345',
		PASS: '12345'
	},
	jwtSecretKey: 'g8b9(-=~Sdf)',
	SALT_ROUNDS: 10
});

module.exports = SERVER;