const DB_MODEL_REF = {
	USER: 'users'
};

const REGEX = {
	EMAIL: /^\w+([.-]\w+)*@\w+([.-]\w+)*\.\w{2,5}$/i
};

const CONSTANT = Object.freeze({
	DB_MODEL_REF,
	REGEX
});

module.exports = CONSTANT;