const userRoute = require('./user');

module.exports = function (app) {
	// Attach user Routes
	app.use('/v1/user', userRoute);
};