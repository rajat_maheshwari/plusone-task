const user = require('../models/user');

async function checkIsUserExists(params) {
	try {
		let query = {};
		query.email = params.email;
		if (params.userId) query._id = { '$not': { '$eq': params.userId } };

		const projection = { createdAt: 0, updatedAt: 0 };
		const options = { lean: true };

		return await user.findOne(query, projection, options);
	} catch (error) {
		throw error;
	}
}

async function createUser(params) {
	try {
		return await user.create(params);
	} catch (error) {
		throw error;
	}
}

async function findUserById(userId) {
	try {
		let query = {};
		query._id = userId;

		const projection = { updatedAt: 0 };
		const options = { lean: true };

		return await user.findOne(query, projection, options);
	} catch (error) {
		throw error;
	}
}

module.exports = {
	checkIsUserExists,
	createUser,
	findUserById
};