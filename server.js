const express = require('express');
const mongoose = require('mongoose');

const { SERVER } = require('./lib/config/index');

const app = express();

// to allow application/json
app.use(express.json());

// database logs
mongoose.set('debug', true);

// attach the routes to the app
require('./lib/route/index')(app);

// database connection
const dbUrl = SERVER.MONGO.DB_URL + SERVER.MONGO.DB_NAME;
mongoose.connect(dbUrl, SERVER.MONGO.OPTIONS).catch(error => {
	console.warn('Failed to connect to mongodb instance.');
});

app.listen(SERVER.PORT, () => {
	console.log(`app is running at ${SERVER.PORT}`);
});